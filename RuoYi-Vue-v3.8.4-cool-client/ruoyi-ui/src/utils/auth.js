import Cookies from 'js-cookie'
import constant from "../constant";


const TokenKey = 'Admin-Token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function setAccessToken(token) {
  return Cookies.set(constant.ACCESS_TOKEN, token)
}
export function getAccessToken() {
  return Cookies.get(constant.ACCESS_TOKEN)
}

export function removeAccessToken() {
  return Cookies.remove(constant.ACCESS_TOKEN)
}
