## 致谢

感谢若依快速开发框架（前后端分离版、微服务版），感谢smart-oatuh2项目，作者[Joe.zhou](https://blog.csdn.net/a466350665)

```
smart-oatuh2项目地址：https://gitee.com/a466350665/smart-sso
若依官网: http://doc.ruoyi.vip/
```

## 介绍

本项目使用Ruoyi-Vue和Ruoyi-Cloud，实现单点登录和oatuh2授权码模式，提供了前后端实现代码，对代码进行优化

使用redis、不受到二级域名cookie限制，支持分布式,对于第一次接触sso单点登录系统的人员有所帮助，借助本项目进行单点登录快速开发

### 关于单点登录一些总结

单点登录凭证传递方法总结：

1.直接在URL中传递认证信息，能跑就行（安全性有问题，截取认证信息，在任何计算机都能进行单点登录）。

2.在二级域名中设置cookie，安全性有保障，但是域名受限制。

3.模仿淘宝，利用iframe+postMessage跨域通信传递认证信息。

4.微博开发者平台将cookie存储在统一登录页面，进行页面回调。



尝试使用过的框架 1.CAS（前后端不分离、改造成本大）、2.xxl-sso(一个很好的单点登录实现框架)3.smart-oatuh2（本项目使用框架），因为业务需求，采取了微博开放者平台的页面跳转实现，如果需要模仿淘宝单点登录系统请使用iframe+postMessage

### 基于iframe+postMessage实现代码

```
客户端:嵌入统一登录页面

<template>
  <div class="login">
    <h3 class="title">A系统统一登陆页面</h3>
    <iframe id="iframe"
            src="http://ssologin.runjian.com:880/ssoLogin"
            width="350px"
            height="350px"
    ></iframe>
  </div>
</template>


<script>
//监听登录页面发送令牌
  window.addEventListener('message',e=>{
    // <!-- 对消息来源origin做一下过滤，避免接收到非法域名的消息导致的xss攻击 -->
    console.log("客户端接受到消息")
    console.log(e)
    if(e.origin==='http://ssologin.runjian.com:880/ssoLogin'){
      console.log(e.origin) //子页面URL
      console.log(e.source) // 子页面window对象，全等于iframe.contentWindow
      console.log(e.data) //子页面发送的消息
    }
  },false)
</script>
```

```
统一登录页面：
	登录按钮被点击，登录函数.then(() => {
		window.parent.postMessage(“认证信息”,'http://client3.com:890/ssoLogin')
	}
```

## 使用文档

https://blog.csdn.net/qq_43751489/article/details/127640796
