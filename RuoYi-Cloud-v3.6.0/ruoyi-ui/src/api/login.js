import request from '@/utils/request'

// 登录方法
export function login(username, password, code, uuid) {
  return request({
    url: '/auth/login',
    headers: {
      isToken: false
    },
    method: 'post',
    data: { username, password, code, uuid }
  })
}

// 注册方法
export function register(data) {
  return request({
    url: '/auth/register',
    headers: {
      isToken: false
    },
    method: 'post',
    data: data
  })
}

// 刷新方法
export function refreshToken() {
  return request({
    url: '/auth/refresh',
    method: 'post'
  })
}

// 获取用户详细信息
export function getInfo() {
  return request({
    url: '/system/user/getInfo',
    method: 'get'
  })
}

// 退出方法
export function logout() {
  return request({
    url: '/auth/logout',
    method: 'delete'
  })
}

// 获取验证码
export function getCodeImg() {
  return request({
    url: '/code',
    headers: {
      isToken: false
    },
    method: 'get',
    timeout: 20000
  })
}






//发起登录获取TGT和code
export function ssoLogin(username, password, code, uuid, redirectUri, appId) {
  return request({
    url: '/sso/login',
    headers: {
      isToken: false
    },
    method: 'post',
    data: { username, password, code, uuid, redirectUri, appId }
  })
}
//如果已经登录会返回cdoe
export function checkSsoLogin(redirectUri, appId) {
  let data = { 'redirectUri': redirectUri, 'appId': appId }
  return request({
    url: '/sso/loginCheck',
    method: 'get',
    params: data,
    headers: {
      isToken: false
    }
  })
}
//退出
export function ssoLogout() {
  return request({
    url: '/sso/logout',
    method: 'get',
    headers: {
      isToken: false
    }
  })
}
