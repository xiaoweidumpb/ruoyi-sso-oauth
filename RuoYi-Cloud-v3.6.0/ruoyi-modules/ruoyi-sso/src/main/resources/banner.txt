Spring Boot Version: ${spring-boot.version}
Spring Application Name: ${spring.application.name}
                            _
                           (_)
 _ __  _   _   ___   _   _  _  ______  ___  ___  ___
| '__|| | | | / _ \ | | | || ||______|/ __|/ __|/ _ \
| |   | |_| || (_) || |_| || |        \__ \\__ \|(_)|
|_|    \__,_| \___/  \__, ||_|        |___/|___/\___/
                      __/ |
                     |___/
