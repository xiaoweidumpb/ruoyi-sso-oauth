package com.ruoyi.sso.service;


/**
 * 基础应用Service接口
 *
 * @author weimingzhong
 * @date 2022-10-22
 */
public interface ICommonApplicationService {



    /**
     * 查询appId和redirectUri是否正确
     *
     * @param redirectUri 回调地址
     * @param appId appId
     * @return 结果
     */
    boolean checkRedirectUriAndAppId(String redirectUri, String appId);

    /**
     * 查询appId和appKey是否正确
     *
     * @param appId appId
     * @param appKey appKey
     * @return 结果
     */
    boolean checkAppIdAndappKey(String appId, String appKey);
}
