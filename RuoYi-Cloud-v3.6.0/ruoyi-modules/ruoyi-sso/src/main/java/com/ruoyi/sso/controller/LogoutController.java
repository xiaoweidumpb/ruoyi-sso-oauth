package com.ruoyi.sso.controller;


import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.sso.service.impl.SessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 单点登出
 *
 * @author Joe
 */
@RestController
@RequestMapping("/logout")
public class LogoutController {

    @Autowired
    private SessionManager sessionManager;

    /**
     * 登出,清除统一登录页面TGT Cookie
     * 相关业务最好放到Server层做
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public AjaxResult logout(
            HttpServletRequest request, HttpServletResponse response) {
        sessionManager.invalidate(request, response);
        return AjaxResult.success();
    }
}
