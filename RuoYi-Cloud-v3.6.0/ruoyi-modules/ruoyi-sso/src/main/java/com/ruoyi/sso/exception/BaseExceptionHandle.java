package com.ruoyi.sso.exception;




import com.ruoyi.common.core.exception.base.BaseException;
import com.ruoyi.common.core.web.domain.AjaxResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * @author weimingzhong
 * BaseException 是框架基础异常类，继承RuntimeException
 * 统一处理自定义异常，将提示消息返回
 */

@RestControllerAdvice(basePackages = "com.rjgf.sso.controller")
public class BaseExceptionHandle {
    @ExceptionHandler(BaseException.class)
    public AjaxResult handleBaseException(BaseException e) {
        return AjaxResult.error(e.getDefaultMessage());
    }
}
