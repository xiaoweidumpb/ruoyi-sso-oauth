package com.ruoyi.sso.service.impl;


import com.ruoyi.sso.service.ICommonApplicationService;
import org.springframework.stereotype.Service;

/**
 * 基础应用Service业务层处理
 *
 * @author weimingzhong
 * @date 2022-10-22
 */
@Service
public class CommonApplicationServiceImpl implements ICommonApplicationService {

//    @Autowired
//    private CommonApplicationMapper commonApplicationMapper;


    /**
     * 查询appId和redirectUri是否正确
     *
     * @param redirectUri 回调地址
     * @param appId       appId
     * @return 结果
     */
    @Override
    public boolean checkRedirectUriAndAppId(String redirectUri, String appId) {
        return true;
    }

    @Override
    public boolean checkAppIdAndappKey(String appId, String appKey) {
        return true;
    }

}
