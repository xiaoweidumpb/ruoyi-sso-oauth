package com.ruoyi.sso.other.constant;

/**
 * @author Joe
 */
public class Oauth2Constant {


	/**
	 * 授权方式
	 */
	public static final String GRANT_TYPE = "grantType";

	/**
	 * 应用唯一标识
	 */
	public static final String APP_ID = "appId";

	/**
	 * 应用密钥
	 */
	public static final String APP_SECRET = "appKey";


	/**
	 * 授权码（授权码模式）
	 */
	public static final String AUTH_CODE = "code";

	/**
	 * 用户名（密码模式）
	 */
	public static final String USERNAME = "username";

	/**
	 * 密码（密码模式）
	 */
	public static final String PASSWORD = "password";

	/**
	 * 授权码模式
	 */
	public static final String AUTHORIZATION_CODE = "authorization_code";

	/**
	 * 密码模式
	 */
	public static final String AUTHORIZATION_PASSWORD = "password";

}
