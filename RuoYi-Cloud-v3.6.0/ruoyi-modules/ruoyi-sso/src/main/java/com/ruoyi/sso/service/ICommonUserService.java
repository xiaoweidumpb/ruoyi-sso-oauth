package com.ruoyi.sso.service;


import com.ruoyi.sso.domain.CommonUser;

/**
 * 基础用户Service接口
 *
 * @author weimingzhong
 * @date 2022-10-21
 */
public interface ICommonUserService {


    /**
     * 获取基础用户
     *
     * @param username 用户名
     * @return 用户信息
     */
    CommonUser selectCommonUserByUserName(String username);
}
