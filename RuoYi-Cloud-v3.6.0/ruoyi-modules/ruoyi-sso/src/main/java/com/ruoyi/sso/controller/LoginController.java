package com.ruoyi.sso.controller;


import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.sso.domain.SsoLoginBody;
import com.ruoyi.sso.other.constant.Oauth2Constant;
import com.ruoyi.sso.other.constant.SsoConstant;
import com.ruoyi.sso.service.ILoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * 单点登录管理
 *
 * @author Joe
 */
@RestController
public class LoginController {

    @Autowired
    private ILoginService loginService;


    /**
     * 统一登录页面登录预检查
     */
    @GetMapping("/loginCheck")
    public AjaxResult loginCheck(@RequestParam(value = SsoConstant.REDIRECT_URI) String redirectUri,
                                 @RequestParam(value = Oauth2Constant.APP_ID) String appId,
                                 HttpServletRequest request) {
        return AjaxResult.success(loginService.loginCheck(redirectUri, appId, request));
    }


    /**
     * 登录提交
     */
    @PostMapping("/login")
    public String login(@Validated @RequestBody SsoLoginBody loginBody,
                        HttpServletRequest request,
                        HttpServletResponse response) {

        return loginService.login(loginBody, request, response);
    }
}
