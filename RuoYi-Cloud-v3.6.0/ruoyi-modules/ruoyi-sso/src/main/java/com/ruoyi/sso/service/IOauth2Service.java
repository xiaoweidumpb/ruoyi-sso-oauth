package com.ruoyi.sso.service;

import com.ruoyi.sso.domain.AccessToken;

/**
 * @author weimingzhong
 * @date 2022/10/26 20:24
 * @remark
 */
public interface IOauth2Service {
    /**
     * 获取accessToken
     *
     * @param grantType oauth2授权方式有四种模式，支持password( 密码模式)、code(授权码授权模式)
     * @param appId     appId
     * @param appKey    appKey
     * @param code      授权码
     * @param username  用户名 密码模式使用
     * @param password  密码 密码模式使用
     * @return AccessToken AccessToken
     */
    AccessToken getAccessToken(String grantType, String appId, String appKey, String code, String username, String password);

}
