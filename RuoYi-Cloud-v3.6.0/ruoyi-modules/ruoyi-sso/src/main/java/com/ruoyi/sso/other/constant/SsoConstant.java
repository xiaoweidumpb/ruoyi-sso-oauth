package com.ruoyi.sso.other.constant;

/**
 * @author Joe
 */
public class SsoConstant {
    /**
     * 服务端回调客户端地址参数名称
     */
    public static final String REDIRECT_URI = "redirectUri";

    /**
     * 服务端单点登出回调客户端登出参数名称
     */
    public static final String LOGOUT_PARAMETER_NAME = "logoutRequest";


    /**
     * 存cookie中TGT名称，和Cas保存一致
     */
    public static final String TGC = "TGC";


    /**
     * 状态码
     */
    public static final String STATUS_CODE = "statusCode";



}
