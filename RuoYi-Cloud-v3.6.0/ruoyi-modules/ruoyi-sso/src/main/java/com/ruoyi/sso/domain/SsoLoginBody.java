package com.ruoyi.sso.domain;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author weimingzhong
 * @date 2022/10/24 9:18
 * @remark
 */
public class SsoLoginBody implements Serializable {

    private static final long serialVersionUID = -133259845904560781L;

    @NotBlank
    private String redirectUri;
    @NotBlank
    private String appId;
    @NotBlank
    private String username;
    @NotBlank
    private String password;

    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public String toString() {
        return "LoginBody{" +
                "redirectUri='" + redirectUri + '\'' +
                ", appId='" + appId + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
