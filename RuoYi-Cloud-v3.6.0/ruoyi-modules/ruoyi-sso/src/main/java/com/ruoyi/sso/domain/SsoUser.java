package com.ruoyi.sso.domain;

import java.io.Serializable;

/**
 * 已登录用户信息
 *
 * @author Joe
 */
public class SsoUser implements Serializable {

    private static final long serialVersionUID = 1764365572138947234L;

    // 登录成功userId
    private Long id;

    // 登录名
    private String username;

    public SsoUser(Long id, String username) {
        this.id = id;
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SsoUser other = (SsoUser) obj;
        if (id == null) {
            return other.id == null;
        } else {
            return id.equals(other.id);
        }
    }
}
