package com.ruoyi.sso.service.impl;



import com.ruoyi.sso.domain.CommonUser;
import com.ruoyi.sso.service.ICommonUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 基础用户Service业务层处理
 *
 * @author weimingzhong
 * @date 2022-10-21
 */
@Service
public class CommonUserServiceImpl implements ICommonUserService {
//    @Autowired
//    private CommonUserMapper commonUserMapper;

    /**
     * 获取基础用户
     *
     * @param username 用户名
     * @return 用户信息
     */
    @Override
    public CommonUser selectCommonUserByUserName(String username) {

//        return commonUserMapper.selectCommonUserByUserName(username);
        CommonUser commonUser = new CommonUser();
        commonUser.setUserId(1L);
        commonUser.setUserName("admin");
        commonUser.setPassword("$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2");
        return commonUser;
    }
}
