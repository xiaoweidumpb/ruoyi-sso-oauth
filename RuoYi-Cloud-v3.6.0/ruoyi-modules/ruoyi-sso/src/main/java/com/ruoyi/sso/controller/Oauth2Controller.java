package com.ruoyi.sso.controller;


import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.sso.other.constant.Oauth2Constant;
import com.ruoyi.sso.service.IOauth2Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Oauth2服务管理
 *
 * @author Joe
 */
@SuppressWarnings("rawtypes")
@RestController
@RequestMapping("/oauth2")
public class Oauth2Controller {

    @Autowired
    private IOauth2Service oauth2Service;

    /**
     * 获取accessToken
     */
    @RequestMapping(value = "/access_token", method = RequestMethod.GET)
    public AjaxResult getAccessToken(
            @RequestParam(value = Oauth2Constant.GRANT_TYPE) String grantType,
            @RequestParam(value = Oauth2Constant.APP_ID) String appId,
            @RequestParam(value = Oauth2Constant.APP_SECRET) String appKey,
            @RequestParam(value = Oauth2Constant.AUTH_CODE, required = false) String code,
            @RequestParam(value = Oauth2Constant.USERNAME, required = false) String username,
            @RequestParam(value = Oauth2Constant.PASSWORD, required = false) String password) {
        return AjaxResult.success(oauth2Service.getAccessToken(grantType, appId, appKey, code, username, password));
    }
}
