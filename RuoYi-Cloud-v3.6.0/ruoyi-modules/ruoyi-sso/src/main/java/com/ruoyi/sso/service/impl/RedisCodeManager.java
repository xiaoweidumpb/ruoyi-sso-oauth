package com.ruoyi.sso.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.sso.domain.CodeContent;
import com.ruoyi.sso.service.CodeManager;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 分布式授权码管理
 *
 * @author Joe
 */
@Component
@Primary
@ConditionalOnProperty(name = "sso.session.manager", havingValue = "redis")
public class RedisCodeManager implements CodeManager {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public void create(String code, CodeContent codeContent) {
        redisTemplate.opsForValue().set(code, JSON.toJSONString(codeContent), getExpiresIn(), TimeUnit.SECONDS);
    }

    @Override
    public CodeContent getAndRemove(String code) {
        String cc = redisTemplate.opsForValue().get(code);
        if (!StringUtils.isEmpty(cc)) {
            redisTemplate.delete(code);
        }
        return JSONObject.parseObject(cc, CodeContent.class);
    }
}
