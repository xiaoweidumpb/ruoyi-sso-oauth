package com.ruoyi.sso.service;

import com.ruoyi.sso.domain.SsoLoginBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author weimingzhong
 * @date 2022/10/26 20:11
 * @remark
 */
public interface ILoginService {

    /**
     * 统一登录页面登录预检查
     *
     * @param request     用于检查是否有tgt
     * @param redirectUri 用于检查redirectUri正确
     * @param appId       用于检查appId正确
     * @return 授权码
     */
    Map<String, Object> loginCheck(String redirectUri, String appId, HttpServletRequest request);

    /**
     * 登录提交
     *
     * @param loginBody 请求体
     * @param request   将tgt存储到cookie中
     * @param response  将tgt存储到cookie中
     * @return 结果
     */
    String login(SsoLoginBody loginBody, HttpServletRequest request, HttpServletResponse response);


}
